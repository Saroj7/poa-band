const { Obi } = require('@bandprotocol/obi.js');
const BandChain = require('@bandprotocol/bandchain.js');

// BandChain devnet endpoint URL
const endpoint = 'http://poa-api.bandchain.org';
// Mnemonic of the account to make the query from.
const mnemonic =
  'puzzle angry glance traffic giraffe grow shoe demand engine space crouch glimpse diet exist fox allow ghost connect vote tennis approve century citizen vocal';

// Request parameters
const oracleScriptID = 10;
const minCount = 3;
const askCount = 4;

(async () => {
  // Instantiating BandChain with REST endpoint
  const bandchain = new BandChain(endpoint);
  // Create an instance of OracleScript with the script ID
  const oracleScript = await bandchain.getOracleScript(oracleScriptID);

  // Get script info
  const schema = oracleScript.schema;

  // Initiate obi object to be used for decoding the result later
  const obi = new Obi(schema);

  // Create a new request, which will block into the tx is confirmed
  try {
    const requestID = await bandchain.submitRequestTx(
      oracleScript,
      { seed:  'hello', time: 1594870858 },
      { minCount, askCount },
      mnemonic
    );

    // Get request proof
    const requestProof = await bandchain.getRequestProof(requestID);

    // Get request result
    const responseStruct = await bandchain.getRequestResult(requestID);
    const encodedOutput = responseStruct.response_packet_data['result'];

    let randomHash = obi.decodeOutput(Buffer.from(encodedOutput))['hash'];

    console.log('Random Hash:');
    console.log(randomHash);
  } catch (e) {
    // Something went wrong (e.g. specified time is in the future)
    console.error('Data request failed with reason: ', e);
  }
})();